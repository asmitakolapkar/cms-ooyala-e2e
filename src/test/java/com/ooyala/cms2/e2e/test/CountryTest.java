package com.ooyala.cms2.e2e.test;

import static org.testng.Assert.assertTrue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.cms.ui.base.TestBase;
import com.ooyala.ui.pages.CountryPage;
import com.ooyala.ui.pages.OylHomePage;
import com.ooyala.ui.pages.OylLoginPage;
import com.vuclip.cms2.Cms2AutomationApplication;
import com.vuclip.cms2.entity.CountryEntity;
import com.vuclip.cms2.helper.CountryHelper;
import com.vuclip.cms2.helper.LanguageHelper;
import com.vuclip.cms2.validator.RefDataValidator;

import io.restassured.response.Response;

@ContextConfiguration(classes={Cms2AutomationApplication.class})
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class CountryTest extends AbstractTestNGSpringContextTests{
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	OylLoginPage oylLoginPage;
	OylHomePage oylhomePage;
	CountryPage countrypage;
	
	@Autowired
	CountryHelper countryHelper;
	
	@Autowired
	RefDataValidator refDataValidator;
	
	Long cmsId = 103L;
	
	public CountryTest(){
		countrypage = new CountryPage();
		TestBase testBase = new TestBase();
	}
	
	@BeforeClass
	public void setup() throws InterruptedException {
		TestBase.initialization("flexUrl");
		oylLoginPage = new OylLoginPage();
		Thread.sleep(2000);
		oylhomePage = oylLoginPage.login(TestBase.prop.getProperty("flexUsername"), TestBase.prop.getProperty("flexPassword"));
		Thread.sleep(2000);
	}
	
	@Test(priority=1, enabled=true)
	public void verifyCreateNewCountry() throws Exception{
		
		try {
			oylhomePage.clickNewButton();
			countrypage= oylhomePage.selectCountryOption();
			//enter form details
			countrypage.enterName("IndiaAutomation");
			countrypage.enterDescription("This is a artist record created by Automation");
			countrypage.enterCmsId(cmsId);
			countrypage.enterA2Code("A122");
			countrypage.enterA3Code("A423");
			countrypage.enterLongName("A A Automation Country 900");
			countrypage.enterCurrency("Rs");
			countrypage.selectRegion("Asia");
			countrypage.clickCreationSavebtn();
			
			//Verify country is created in ooyala
			log.info("****** Now wil click on country tab ******");
			countrypage = oylhomePage.clickCountryTab();
			log.info("**In test class..Passed out of select countryTab**");
			Assert.assertTrue(countrypage.checkCountryPresent("IndiaAutomation"));
		
			Thread.sleep(5000);
			// validate country is created in cms db
			CountryEntity country = countryHelper.getCountry(cmsId);

			Response response = countryHelper.getExpectedTestData("country_create", 1);

			refDataValidator.validateCountryService(country, response);
			
			//get country by id
			Response rsp = countryHelper.getCountryById(cmsId);
			
			Response expectedResponse = countryHelper.getExpectedTestData("country_get", 1);
			
			refDataValidator.validateResponse(expectedResponse, rsp);

		} catch (Exception e) {
			assertTrue(false, e.getMessage());

			logger.error("Exception>>>>>>>>>", e);
		}
	}
	
	@Test(priority=2, enabled=false)
	public void verifyUpdateCountry() throws Exception{
		
		try {
			countrypage.OpenCountry("IndiaAutomation");
			countrypage.clickEditBtn();
			countrypage.enterName("IndiaAutomation1");
			countrypage.clickUpdationSavebtn();
			Thread.sleep(2000);
			
			//verify ooyala
			countrypage = oylhomePage.clickCountryTab();
			log.info("******Clicked country tab to verify update********");
			Assert.assertTrue(countrypage.checkCountryPresent("IndiaAutomation1"));
			Thread.sleep(5000);
			
			// validate cms db
			CountryEntity country = countryHelper.getCountry(cmsId);

			Response response = countryHelper.getExpectedTestData("country_update", 1);

			refDataValidator.validateCountryService(country, response);

		} catch (Exception e) {
			assertTrue(false, e.getMessage());

			logger.error("Exception>>>>>>>>>", e);
		}
	}
		
	@Test(priority=3, enabled=false)
	public void verifyDeleteCountry() throws Exception{
		try {
			
			countrypage.OpenCountry("IndiaAutomation1");
			countrypage.clickAndConfirmDelete();
			log.info("*******************Country deleted successfully***********");
			Assert.assertFalse(countrypage.checkCountryPresent("IndiaAutomation1"));
		
			Thread.sleep(5000);
			// validate cms db
			CountryEntity country = countryHelper.getCountry(cmsId);

			Response response = countryHelper.getExpectedTestData("country_delete", 1);

			refDataValidator.validateCountryService(country, response);

		} catch (Exception e) {
			assertTrue(false, e.getMessage());

			logger.error("Exception>>>>>>>>>", e);
		}
	}
		
	@AfterClass
	public void tearDown(){
		TestBase.driver.quit();
	}
	
}
