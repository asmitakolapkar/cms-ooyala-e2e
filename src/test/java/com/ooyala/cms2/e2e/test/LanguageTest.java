package com.ooyala.cms2.e2e.test;

import static org.testng.Assert.assertTrue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.cms.ui.base.TestBase;
import com.ooyala.ui.pages.CountryPage;
import com.ooyala.ui.pages.CpPage;
import com.ooyala.ui.pages.LanguagePage;
import com.ooyala.ui.pages.OylHomePage;
import com.ooyala.ui.pages.OylLoginPage;
import com.vuclip.cms2.Cms2AutomationApplication;
import com.vuclip.cms2.entity.CountryEntity;
import com.vuclip.cms2.entity.LanguageEntity;
import com.vuclip.cms2.helper.LanguageHelper;
import com.vuclip.cms2.validator.RefDataValidator;

import io.restassured.response.Response;

@ContextConfiguration(classes={Cms2AutomationApplication.class})
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class LanguageTest extends AbstractTestNGSpringContextTests{
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	OylLoginPage oylLoginPage;
	OylHomePage oylhomePage;
	LanguagePage languagePage;
	
	@Autowired
	LanguageHelper languageHelper;
	
	@Autowired
	RefDataValidator refDataValidator;
	
	Long cmsId = 103L;
	
	String code = "LnagCode";
	
	public LanguageTest(){
		languagePage = new LanguagePage();
		TestBase testBase = new TestBase();
	}
	
	@BeforeClass
	public void setup() throws InterruptedException {
		TestBase.initialization("flexUrl");
		oylLoginPage = new OylLoginPage();
		Thread.sleep(2000);
		oylhomePage = oylLoginPage.login(TestBase.prop.getProperty("flexUsername"), TestBase.prop.getProperty("flexPassword"));
		Thread.sleep(2000);
	}
	
	@Test(priority=1)
	public void verifyCreateNewLangauge() throws Exception{
		try {
			
			oylhomePage.clickNewButton();
			languagePage= oylhomePage.selectLanguageOption();
			//enter form details
			languagePage.enterLanguageName("Automation");
			languagePage.enterDescription("This language is created through automation");
			languagePage.enterCmsId(cmsId);
			languagePage.enterDisplayName("Automation");
			languagePage.enterLangCode(code);
			languagePage.saveLanguageRecord();
		
			//Verify if language created in ooyala
			languagePage= oylhomePage.clickLanguageTab();
			Assert.assertTrue(languagePage.checkLanguageCreated("Automation"));
		
			// verify cms db
			LanguageEntity lang = languageHelper.getLang(cmsId);
			
			Response response = languageHelper.getExpectedTestData("language_create", 1);

			refDataValidator.validateLanguageService(lang, response);
			
			// get lang by id
			Response rspbyId = languageHelper.getLanguageById(cmsId);
			
			Response expectedResponse = languageHelper.getExpectedTestData("language_get", 1);
			
			refDataValidator.validateResponse(expectedResponse, rspbyId);
			
			// get lang by code 
			Response rspbyCode = languageHelper.getLanguageByCode(code);
			
			refDataValidator.validateResponse(expectedResponse, rspbyCode);

		} catch (Exception e) {
			assertTrue(false, e.getMessage());

			logger.error("Exception>>>>>>>>>", e);
		}
	}
	
    // Method to verify edit country record
	@Test(priority=2,enabled=false)
	public void verifyUpdateCountry() throws Exception{
		try {
			
			languagePage.OpenLangauge("Automation");
			languagePage.clickEditBtn();
			languagePage.enterLanguageName("AutomationTest");
			languagePage.clickUpdationSavebtn();
			Thread.sleep(2000);
			//Verify if langauge record is updated
			languagePage = oylhomePage.clickLanguageTab();
			log.info("******Clicked language tab to verify update********");
			Assert.assertTrue(languagePage.checkLanguageCreated("AutomationTest"));
			
			LanguageEntity lang = languageHelper.getLang(cmsId);
			
			Response response = languageHelper.getExpectedTestData("language_update", 1);

			refDataValidator.validateLanguageService(lang, response);

		} catch (Exception e) {
			assertTrue(false, e.getMessage());

			logger.error("Exception>>>>>>>>>", e);
		}
	}
	
	// Method to verify delete language record
	@Test(priority=3,enabled=false)
	public void verifyDeleteCountry() throws Exception{
		try {
			
			languagePage.OpenLangauge("AutomationTest");
			languagePage.clickAndConfirmDelete();
			log.info("*******************Language deleted successfully***********");
			Thread.sleep(4000);
			Assert.assertFalse(languagePage.checkLanguageCreated("AutomationTest"));

			LanguageEntity lang = languageHelper.getLang(cmsId);
			
			Response response = languageHelper.getExpectedTestData("language_delete", 1);

			refDataValidator.validateLanguageService(lang, response);

		} catch (Exception e) {
			assertTrue(false, e.getMessage());

			logger.error("Exception>>>>>>>>>", e);
		}
	}
		
	@AfterClass
	public void tearDown(){
		TestBase.driver.quit();
	}
	
}

