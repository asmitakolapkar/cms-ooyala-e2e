package com.ooyala.cms2.e2e.test;


import static org.testng.Assert.assertTrue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.cms.ui.base.TestBase;
import com.ooyala.ui.pages.CpPage;
import com.ooyala.ui.pages.OylHomePage;
import com.ooyala.ui.pages.OylLoginPage;
import com.vuclip.cms2.Cms2AutomationApplication;
import com.vuclip.cms2.helper.CPHelper;
import com.vuclip.cms2.validator.CPValidator;

import io.restassured.response.Response;

@ContextConfiguration(classes={Cms2AutomationApplication.class})
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class CpTest extends AbstractTestNGSpringContextTests{
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	OylLoginPage oylLoginPage;
	OylHomePage oylhomePage;
	CpPage cpPage;

	@Autowired
	CPHelper cpHelper;

	@Autowired
	CPValidator cpValidator;

	Long rmsId = 9901L;
	
	public CpTest(){
		cpPage = new CpPage();
		TestBase testBase = new TestBase();
	}
	
	@BeforeClass
	public void setup() throws InterruptedException {
		CpPage.initialization("flexUrl");
		oylLoginPage = new OylLoginPage();
		Thread.sleep(2000);
		oylhomePage = oylLoginPage.login(CpPage.prop.getProperty("flexUsername"), CpPage.prop.getProperty("flexPassword"));
		Thread.sleep(2000);
	}
	
	@Test(priority=1, enabled=true, groups = "CP")
	public void verifyCreateNewCp() throws Exception{
		try {
			
			oylhomePage.clickNewButton();
			cpPage= oylhomePage.selectcpOption();
			//enter form details
			cpPage.enterName("TestAutomation");
			cpPage.enterDescription("Created cp using automation");
			cpPage.enterRmsId(rmsId);
			cpPage.enterCmsId(1000);
			cpPage.selectProfile("Profile1");
			cpPage.enterBatonName("Test");
			cpPage.enterMngrName("testManager");
			cpPage.entercpUserName("test username");
			cpPage.enterSecMngrName("test SecUsername");
			cpPage.enterCpCountry("India");
			cpPage.enterSpocName("Spoc name");
			cpPage.enterSpocEmail("testmail@mail.in");
			cpPage.enterContact("1234567890");
			cpPage.enterOpsSpocName("SpocName");
			cpPage.enterOpsSpocEmail("SpocEmail@mail.in");
			cpPage.enterOpsContact("4567891234");
			cpPage.activateCp();
			
			//save record
			cpPage.saveCpRecord();
			cpPage= oylhomePage.clickCpTab();
			Assert.assertTrue(cpPage.checkCpCreated("TestAutomation"));
			
			Thread.sleep(5000);
			// Call to test data service for expected
			Response response = cpHelper.getExpectedTestData("cp_create", 1);
	
			// Validate product push data against service response
			cpValidator.validateCpService(rmsId, response);
			
		} catch (Exception e) {
			assertTrue(false, e.getMessage());
	
			log.error("Exception>>>>>>>>>", e);
		}
	}
	
	// Method to verify update cp record
	@Test(priority=2, enabled=false, groups = "CP")
	public void verifyUpdateCountry() throws Exception{
		try {
			cpPage.OpenCp("TestAutomation");
			cpPage.clickEditBtn();
			cpPage.enterName("TestAutomation Update");
			cpPage.clickUpdationSavebtn();
			Thread.sleep(2000);
			//Verify if cp record is updated
			cpPage = oylhomePage.clickCpTab();
			log.info("******Clicked country tab to verify update********");
			Assert.assertTrue(cpPage.checkCpCreated("TestAutomation Update"));
			
			Thread.sleep(5000);
			
			Response response = cpHelper.getExpectedTestData("cp_update", 1);
	
			cpValidator.validateCpService(rmsId, response);
			
		} catch (Exception e) {
			assertTrue(false, e.getMessage());
	
			log.error("Exception>>>>>>>>>", e);
		}
	}
	
	// Method to verify delete cp record
	@Test(priority=3, groups = "CP", enabled=false)
	public void verifyDeleteCountry() throws Exception{
		try { 
			cpPage.OpenCp("TestAutomation Update");
			cpPage.clickAndConfirmDelete();
			log.info("*******************Language deleted successfully***********");
			Thread.sleep(4000);
			Assert.assertFalse(cpPage.checkCpCreated("TestAutomation Update"));
			
			Thread.sleep(5000);
			
			Response response = cpHelper.getExpectedTestData("cp_delete", 1);

			cpValidator.validateCpService(rmsId, response);
			
		} catch (Exception e) {
			assertTrue(false, e.getMessage());
	
			log.error("Exception>>>>>>>>>", e);
		}
		
	}
	
	@AfterClass
	public void tearDown(){
		CpPage.driver.quit();
	}
}
