package com.ooyala.qa.testcases;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ooyala.ui.pages.CountryPage;
import com.ooyala.ui.pages.LanguagePage;
import com.ooyala.ui.pages.OylHomePage;
import com.ooyala.ui.pages.OylLoginPage;

public class LanguageTest extends LanguagePage{
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	OylLoginPage oylLoginPage;
	OylHomePage oylhomePage;
	LanguagePage languagePage;
	
	public LanguageTest(){
		super();
	}
	
	@BeforeClass
	public void setup() throws InterruptedException {
		initialization("flexUrl");
		oylLoginPage = new OylLoginPage();
		Thread.sleep(2000);
		oylhomePage = oylLoginPage.login(prop.getProperty("flexUsername"), prop.getProperty("flexPassword"));
		Thread.sleep(2000);
	}
	
	@Test(priority=1)
	public void verifyCreateNewLangauge() throws Exception{
		
		oylhomePage.clickNewButton();
		languagePage= oylhomePage.selectLanguageOption();
		//enter form details
		languagePage.enterLanguageName("Automation");
		languagePage.enterDescription("This language is created through automation");
		languagePage.enterCmsId(16000);
		languagePage.enterDisplayName("Automation");
		languagePage.enterLangCode("A01");
		
		//save the record for creation
		languagePage.saveLanguageRecord();
		//Thread.sleep(2000);
		//Verify if language created
		languagePage= oylhomePage.clickLanguageTab();
		Assert.assertTrue(checkLanguageCreated("Automation"));
	}
	
    // Method to verify edit country record
	@Test(priority=2)
	public void verifyUpdateCountry() throws Exception{
		languagePage.OpenLangauge("Automation");
		languagePage.clickEditBtn();
		languagePage.enterLanguageName("AutomationTest");
		languagePage.clickUpdationSavebtn();
		Thread.sleep(2000);
		//Verify if langauge record is updated
		languagePage = oylhomePage.clickLanguageTab();
		log.info("******Clicked language tab to verify update********");
		Assert.assertTrue(checkLanguageCreated("AutomationTest"));
	}
	
	// Method to verify delete language record
	@Test(priority=3)
	public void verifyDeleteCountry() throws Exception{
		languagePage.OpenLangauge("AutomationTest");
		languagePage.clickAndConfirmDelete();
		log.info("*******************Language deleted successfully***********");
		Thread.sleep(4000);
		Assert.assertFalse(checkLanguageCreated("AutomationTest"));	
	}
		
	@AfterClass
	public void tearDown(){
		driver.quit();
	}
	
}

