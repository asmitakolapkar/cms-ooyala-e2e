package com.ooyala.qa.testcases;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ooyala.ui.pages.CpPage;
import com.ooyala.ui.pages.OylHomePage;
import com.ooyala.ui.pages.OylLoginPage;

public class CpTest extends CpPage{
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	OylLoginPage oylLoginPage;
	OylHomePage oylhomePage;
	CpPage cpPage;
	
	public CpTest(){
		super();
	}
	
	@BeforeClass
	public void setup() throws InterruptedException {
		initialization("flexUrl");
		oylLoginPage = new OylLoginPage();
		Thread.sleep(2000);
		oylhomePage = oylLoginPage.login(prop.getProperty("flexUsername"), prop.getProperty("flexPassword"));
		Thread.sleep(2000);
	}
	
	@Test(priority=1, enabled=true, groups = "cpCreate")
	public void verifyCreateNewCp() throws Exception{
		
		oylhomePage.clickNewButton();
		cpPage= oylhomePage.selectcpOption();
		//enter form details
		cpPage.enterName("TestAutomation");
		cpPage.enterDescription("Created cp using automation");
		cpPage.enterRmsId(1000);
		cpPage.enterCmsId(1000);
		cpPage.selectProfile("Profile1");
		cpPage.enterBatonName("Test");
		cpPage.enterMngrName("testManager");
		cpPage.entercpUserName("test username");
		cpPage.enterSecMngrName("test SecUsername");
		cpPage.enterCpCountry("India");
		cpPage.enterSpocName("Spoc name");
		cpPage.enterSpocEmail("testmail@mail.in");
		cpPage.enterContact("1234567890");
		cpPage.enterOpsSpocName("SpocName");
		cpPage.enterOpsSpocEmail("SpocEmail@mail.in");
		cpPage.enterOpsContact("4567891234");
		cpPage.activateCp();
		
		//save record
		cpPage.saveCpRecord();
		//Thread.sleep(2000);
		//verify record creation
		cpPage= oylhomePage.clickCpTab();
		Assert.assertTrue(checkCpCreated("TestAutomation"));
	}
	
	// Method to verify update cp record
	@Test(priority=2, enabled=true)
	public void verifyUpdateCountry() throws Exception{
		cpPage.OpenCp("TestAutomation");
		cpPage.clickEditBtn();
		cpPage.enterName("TestAutomation Update");
		cpPage.clickUpdationSavebtn();
		Thread.sleep(2000);
		//Verify if cp record is updated
		cpPage = oylhomePage.clickCpTab();
		log.info("******Clicked country tab to verify update********");
		Assert.assertTrue(checkCpCreated("TestAutomation Update"));
	}
	
	// Method to verify delete cp record
	@Test(priority=3)
	public void verifyDeleteCountry() throws Exception{
		cpPage.OpenCp("TestAutomation Update");
		cpPage.clickAndConfirmDelete();
		log.info("*******************Language deleted successfully***********");
		Thread.sleep(4000);
		Assert.assertFalse(checkCpCreated("TestAutomation Update"));
		
	}
	
	@AfterClass
	public void tearDown(){
		driver.quit();
	}
}
