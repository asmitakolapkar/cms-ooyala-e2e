package com.ooyala.qa.testcases;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.ooyala.ui.pages.CountryPage;
import com.ooyala.ui.pages.OylHomePage;
import com.ooyala.ui.pages.OylLoginPage;

public class CountryTest extends CountryPage{
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	OylLoginPage oylLoginPage;
	OylHomePage oylhomePage;
	CountryPage countrypage;
	
	public CountryTest(){
		super();
	}
	
	@BeforeClass
	public void setup() throws InterruptedException {
		initialization("flexUrl");
		oylLoginPage = new OylLoginPage();
		Thread.sleep(2000);
		oylhomePage = oylLoginPage.login(prop.getProperty("flexUsername"), prop.getProperty("flexPassword"));
		Thread.sleep(2000);
	}
	
	@Test(priority=1, enabled=true)
	public void verifyCreateNewCountry() throws Exception{
		oylhomePage.clickNewButton();
		countrypage= oylhomePage.selectCountryOption();
		//enter form details
		countrypage.enterName("IndiaAutomation");
		countrypage.enterDescription("This is a artist record created by Automation");
		countrypage.enterCmsId(900);
		countrypage.enterA2Code("A122");
		countrypage.enterA3Code("A423");
		countrypage.enterLongName("A A Automation Country 900");
		countrypage.enterCurrency("Rs");
		countrypage.selectRegion("Asia");
		
		//save the record
		countrypage.clickCreationSavebtn();
		//Verify if country record is created
		log.info("******Now wil click on country tab******");
		countrypage = oylhomePage.clickCountryTab();
		log.info("**In test class..Passed out of select countryTab**");
		Assert.assertTrue(checkCountryPresent("IndiaAutomation"));
	}
	
	
	// Method to verify edit country record
	@Test(priority=2, enabled=true)
	public void verifyUpdateCountry() throws Exception{
		countrypage.OpenCountry("IndiaAutomation");
		countrypage.clickEditBtn();
		countrypage.enterName("IndiaAutomation1");
		countrypage.clickUpdationSavebtn();
		Thread.sleep(2000);
		//Verify if country record is updated
		countrypage = oylhomePage.clickCountryTab();
		log.info("******Clicked country tab to verify update********");
		Assert.assertTrue(checkCountryPresent("IndiaAutomation1"));
	}
		
	// Method to verify delete country record
	@Test(priority=3, enabled=true)
	public void verifyDeleteCountry() throws Exception{
		
		countrypage.OpenCountry("IndiaAutomation1");
		countrypage.clickAndConfirmDelete();
		log.info("*******************Country deleted successfully***********");
		Assert.assertFalse(checkCountryPresent("IndiaAutomation1"));
	}
		
	@AfterClass
	public void tearDown(){
		driver.quit();
	}
	
}
