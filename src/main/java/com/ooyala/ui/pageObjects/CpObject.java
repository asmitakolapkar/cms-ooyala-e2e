package com.ooyala.ui.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cms.ui.base.TestBase;

public class CpObject extends TestBase{
	
	//form fields
	@FindBy(name = "name")
	public
	WebElement cpName;
	
	@FindBy(name = "description")
	public
	WebElement description;
	
	@FindBy(name = "____mio_field____root:rms-cp-id[0]")
	public
	WebElement rmsCpId;
	
	@FindBy(name = "____mio_field____root:cms-cp-id[1]")
	public
	WebElement cmsCpId;
	
	@FindBy(xpath = "//*[@value='Profile1']")
	public
	WebElement batonProfile1;
	
	@FindBy(xpath = "//*[@value='Profile2']")
	public
	WebElement batonProfile2;
	
	@FindBy()
	public
	WebElement batonProfileRadioBtn;
	
	@FindBy(name = "____mio_field____root:baton-profile-name[3]")
	public
	WebElement batonPrName;
	
	@FindBy(name = "____mio_field____root:content-provider-name[4]")
	public
	WebElement contentProviderName;
	
	@FindBy(name = "____mio_field____root:content-user-name[5]")
	public
	WebElement contentUserName;
	
	@FindBy(name = "____mio_field____root:vuclip-cp-manager-name[6]")
	public
	WebElement cpMngrName;
	
	@FindBy(name = "____mio_field____root:secondary-cp-manager-name[7]")
	public
	WebElement secCpMngrName;
	
	@FindBy(name = "____mio_field____root:cp-country[8]")
	public
	WebElement cpCountry;
	
	@FindBy(name = "____mio_field____root:cp-spoc-name[9]")
	public
	WebElement cpSpocName;
	
	@FindBy(name = "____mio_field____root:cp-email-id[10]")
	public
	WebElement cpEmail;
	
	@FindBy(name = "____mio_field____root:cp-contact-no[11]")
	public
	WebElement cpContactNo;
	
	@FindBy(name="____mio_field____root:cp-ops-spoc-name[12]")
	public
	WebElement cpOpsSpocName;
	
	@FindBy(name="____mio_field____root:cp-ops-spoc-email-id[13]")
	public
	WebElement cpOpsSpocEmail;
	
	@FindBy(name="____mio_field____root:cp-ops-contact-no[14]")
	public
	WebElement cpOpsContact;
	
	@FindBy(name="____mio_field____root:is-active[15]")
	public
	WebElement cpIsActive;
	
	@FindBy(id="submit_control-2")
	public
	WebElement saveBtnCreate;
	
	@FindBy(id = "edit_summary")
	protected
	WebElement editButton;
	
	@FindBy(id = "submit_control_bottom")
	protected
	WebElement saveBtnUpdate;
	
	@FindBy(id = "delete")
	public
	WebElement deleteBtn;
	
	@FindBy(id = "yes_control")
	public
	WebElement confirmDelete;
	
	public static WebElement checkSummaryTable(String cpName){
		   return driver.findElement(By.xpath("//*[@id='userobject_search_content-provider_datatable']//table//tbody[2]/tr/td[4]/div[contains(text(),'"+cpName+"')]"));
	}
	
	public static WebElement selectbatonprofileRadioBtn(String pname) {
			return driver.findElement(By.xpath("//*[contains(@class,'baton-profile')]//td[1][contains(text(),'"+pname+"')]/following-sibling::td"));
	}
	
}
