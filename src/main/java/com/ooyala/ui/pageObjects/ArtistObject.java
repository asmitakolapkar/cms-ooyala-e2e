package com.ooyala.ui.pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cms.ui.base.TestBase;

public class ArtistObject extends TestBase {
	
	//Artist Locators
	@FindBy(xpath = "//input[@name='name']")
	protected
	WebElement artistName;
	
	@FindBy(xpath = "//textarea[@name='description']")
	protected
	WebElement artistDescription;
	
	@FindBy(xpath = "//input[@id='120001_cms-artist-id_0']")
	protected
	WebElement artistCMSID;

	@FindBy(xpath = "//button[@id='submit_control-2-button']")
	protected
	WebElement artistSAVE;
	
	
}