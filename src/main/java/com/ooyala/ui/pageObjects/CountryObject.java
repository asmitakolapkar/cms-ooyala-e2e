package com.ooyala.ui.pageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cms.ui.base.TestBase;

public class CountryObject extends TestBase {
	
	
	
	//Country form fields
	@FindBy(xpath = "//*[@name='name']")
	public
	WebElement name;
	
	@FindBy(xpath = "//*[@name='description']")
	public
	WebElement description;
	
	@FindBy(xpath = "//*[@name='____mio_field____root:cms-country-id[0]']")
	public
	WebElement cmsId;
	
	@FindBy(name="____mio_field____root:a2code[1]")
	public
	WebElement a2Code;
	
	@FindBy(name="____mio_field____root:a3code[2]")
	public
	WebElement a3Code;
	
	@FindBy(name="____mio_field____root:currency[3]")
	public
	WebElement currency;
	
	@FindBy(name="____mio_field____root:long-name[4]")
	public
	WebElement longName;
	
	@FindBy(name = "____mio_field____root:region[5]")
	public
	WebElement region;
	
	
	
	//While creation
	@FindBy(xpath = "//*[@id='submit_control-2-button']")
	protected
	WebElement saveButtonCreate;
	
	//*************Update Locators*************//
	@FindBy(id = "edit_summary")
	protected
	WebElement editButton;
	
	@FindBy(id = "submit_control_bottom")
	protected
	WebElement saveBtnUpdate;
	
	//*************Delete Locators*************//
	
	@FindBy(id = "delete")
	public
	WebElement deleteBtn;
	
	@FindBy(id = "dialog_popup")
	public
	WebElement deleteAlert;
	
	@FindBy(id = "yes_control")
	public
	WebElement confirmDelete;
	
	@FindBy(id = "search_text")
	public
	WebElement searchInput;
	
	public static WebElement checkSummaryTable(String cname){
		   return driver.findElement(By.xpath("//*[@id='userobject_search_country_datatable']//table//tbody//tr/td[4]/div[contains(text(),'"+cname+"')]"));
	}
	
	
	}
