package com.ooyala.ui.pages;

import org.openqa.selenium.support.PageFactory;

import com.ooyala.ui.pageObjects.ArtistObject;


public class ArtistPage extends ArtistObject {
		
		// Initializing the Page Objects:
		public ArtistPage() {
			PageFactory.initElements(driver, this);
		}
		
		//Actions
		
		public void setName(String name) {
			artistName.sendKeys(name);
		}
		
		public void setDescription(String descp) {
			artistDescription.sendKeys(descp);
		}
		
		public void setCmsId(String id) {
			artistCMSID.sendKeys(id);
		}
		
		public void saveButtonClick() throws InterruptedException {
			artistSAVE.click();
			Thread.sleep(5000);
		}
}
