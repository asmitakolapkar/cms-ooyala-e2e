package com.ooyala.ui.pages;


import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.openqa.selenium.support.PageFactory;
import com.ooyala.ui.pageObjects.OylHomePageObject;

@Component
public class OylHomePage extends OylHomePageObject{
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	// Initializing the Page Objects:
		public OylHomePage() {
			PageFactory.initElements(driver, this);
		}
		
		public void clickNewButton() {
			newButton.click();
		}
		
		//select country option in rightmost new column
		public CountryPage selectCountryOption() {
			
			countryOption.click();
			log.info("*****selected coutry option********");
			return new CountryPage();
		}
		
		public ArtistPage selectArtistOption() {
			
		//	countryOption.click();
			log.info("*****selected coutry option********");
			return new ArtistPage();
		}
		
		//select country tab from tab header
		public CountryPage clickCountryTab() {
			
			countryTab.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			log.info("*****selected coutry tab********");
			return new CountryPage();
		}
		
		//select language option in rightmost new column
		public LanguagePage selectLanguageOption() {
			languageOption.click();
			log.info("*****selected languuag option********");
			return new LanguagePage();
		}
		
		//select language tab from tab header
		public LanguagePage clickLanguageTab() {
			waitForElementToBeClickable(languageTab);
			languageTab.click();
			log.info("*****selected language tab********");
			return new LanguagePage();
		}
		
		//select cp option in rightmost new column
		public CpPage selectcpOption() {
			cpOption.click();
			log.info("*****selected cp option********");
			return new CpPage();
		}
		
		//select language tab from tab header
		public CpPage clickCpTab() {
			waitForElementToBeClickable(cpTab);
			cpTab.click();
			log.info("*****selected cp tab********");
			return new CpPage();
		}
		
		public void inputCountrySearchText(String searchCountryVal) {
			searchbox.sendKeys(searchCountryVal);
		}
		
		public void clickSearchButton() {
			searchBtn.click();	
		}
}
