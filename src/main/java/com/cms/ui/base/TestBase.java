package com.cms.ui.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.vuclip.cms2.util.TestUtil;
import com.vuclip.cms2.util.WebEventListener;

/**
 * @author Sidharth Shukla
 *
 *         
 */
public class TestBase {
	
	public static WebDriver driver;
	public static Properties prop;
	public static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
	public static String projectpath=System.getProperty("user.dir");
	
	
	public TestBase(){
		//String projectpath= System.getProperty("user.dir");
		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream(projectpath+ "/src/main/resources"
					+ "/config.properties");
			prop.load(ip);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void initialization(String url){
		String browserName = prop.getProperty("browser");
		String osName = System.getProperty("os.name");
		System.out.println("Os name"+osName);
		if(osName.contains("Mac")) {
			System.setProperty("webdriver.chrome.driver", projectpath + "/src/main/resources/drivers"+"/chromedriver");	
			driver = new ChromeDriver(); 
		}else {
		if(browserName.equals("chrome")){
			System.setProperty("webdriver.chrome.driver", projectpath + "/src/main/resources/drivers"+"/chromedriver.exe");	
			driver = new ChromeDriver(); 
		}
		else if(browserName.equals("FF")){
			System.setProperty("webdriver.gecko.driver", projectpath+ "/src/main/resources/drivers"+"/geckodriver.exe");	
			driver = new FirefoxDriver(); 
		}
		}
		
		e_driver = new EventFiringWebDriver(driver);
		// Now create object of EventListerHandler to register it with EventFiringWebDriver
		eventListener = new WebEventListener();
		e_driver.register(eventListener);
		driver = e_driver;
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
		
		driver.get(prop.getProperty(url));
		
	}
	

		public static String waitForElementNotVisible(int timeOutInSeconds, WebDriver driver, String elementXPath) {
		    if ((driver == null) || (elementXPath == null) || elementXPath.isEmpty()) {

		        return "Wrong usage of WaitforElementNotVisible()";
		    }
		    try {
		        (new WebDriverWait(driver, timeOutInSeconds)).until(ExpectedConditions.invisibilityOfElementLocated(By
		                .xpath(elementXPath)));
		        return null;
		    } catch (TimeoutException e) {
		        return "Build your own errormessage...";
		    }
		
		
		
		
	}
	
	public void waitForElement(WebElement element) {
		
		 WebDriverWait explicitWait = new WebDriverWait(driver, 10);
		  explicitWait.until(ExpectedConditions.visibilityOf(element));
	}
	
	public void waitForElementToBeClickable(WebElement element) {
		
		 WebDriverWait explicitWait = new WebDriverWait(driver, 20);
		  explicitWait.until(ExpectedConditions.elementToBeClickable(element));
	}
	

	public void waitForElements(List<WebElement> element) {
		
		 WebDriverWait explicitWait = new WebDriverWait(driver, 10);
		  explicitWait.until(ExpectedConditions.visibilityOfAllElements(element));
	}
	
	
	public void moveToElementAndClick(WebElement element) {
		
		Actions actions = new Actions(driver);

		try {
		actions.moveToElement(element).click().perform();
		}
		catch (Exception e) {

			System.out.println("---Element is not present---");
			
			
		}
		
		
	}
	
	public void moveTo(WebElement element) {
		
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		
		
	}
	

	
	
	

}
