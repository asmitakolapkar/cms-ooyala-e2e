package com.vuclip.cms2.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestData {

	private String requestData;

	private int serialNumber;

}
