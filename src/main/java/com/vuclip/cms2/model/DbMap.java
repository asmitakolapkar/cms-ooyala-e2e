package com.vuclip.cms2.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class DbMap {

	private String key;
	private Object value;
	private String type;
	private List<String> sqlList;

}
