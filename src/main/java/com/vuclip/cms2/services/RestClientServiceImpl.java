package com.vuclip.cms2.services;

import static io.restassured.RestAssured.given;

import java.util.HashMap;

import org.apache.http.protocol.HTTP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.vuclip.cms2.util.ConfigProperties;

import io.restassured.response.Response;

@Component
public class RestClientServiceImpl implements RestClientService {

	@Autowired
	ConfigProperties configProperties;

	@Override
	public Response pushDataToPubsubTopic(String topicName, Object message) {

		Gson gson = new Gson();

		Response response = given().pathParam("topicName", topicName).body(gson.toJson(message)).when()
				.contentType("application/json")
				.post(configProperties.getHost().getMockHost() + "/pubsub/publish/{topicName}");

		return response;
	}

	@Override
	public Response getShowService(Long showId) {
		Response response = given().header(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8")
				.queryParam("showId", showId).when().contentType("application/json;charset=UTF-8")
				.get(configProperties.getHost().getCatalogHost() + "/cms/catalogService/show").then().extract()
				.response();

		return response;
	}

	@Override
	public Response getConsumerPubsubMessage(HashMap<String, Object> queryParams) {

		Response response = given().header(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8").queryParams(queryParams)
				.when().contentType("application/json;charset=UTF-8")
				.get(configProperties.getHost().getMockHost() + "/content/pubsub/message").then().extract().response();

		return response;
	}

	@Override
	public Response getCPService(Long id) {

		Response response = given().header(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8").pathParam("id", id)
				.when().contentType("application/json;charset=UTF-8")
				.get(configProperties.getHost().getCpHost() + "/api/cp/{id}").then().extract().response();

		return response;
	}

	@Override
	public Response removePubsubMessage(HashMap<String, Object> parametersMap) {

		Response response = given().queryParams(parametersMap).when()
				.delete(configProperties.getHost().getMockHost() + "/content/pubsub/message").then().extract()
				.response();

		return response;
	}

	@Override
	public Response getInputTestData(String tcName) {

		Response response = given().header(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8")
				.queryParam("tcname", tcName).when().contentType("application/json;charset=UTF-8")
				.get(configProperties.getHost().getTestDataHost() + "/cms/testdata/request").then().extract()
				.response();

		return response;

	}

	@Override
	public Response getExpectedTestData(String tcName, int serialNumber) {
		HashMap<String, Object> queryParams = new HashMap<>();

		queryParams.put("tcname", tcName);
		queryParams.put("serialnumber", String.valueOf(serialNumber));

		Response response = given().header(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8").queryParams(queryParams)
				.when().contentType("application/json;charset=UTF-8")
				.get(configProperties.getHost().getTestDataHost() + "/cms/testdata/response").then().extract()
				.response();

		return response;
	}

	@Override
	public Response getPrerequisitiesSql(String tcName) {

		// RestAssured.defaultParser = Parser.JSON;
		
		Response response = given().header(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8")
				.queryParam("tcname", tcName).when().contentType("application/json;charset=UTF-8")
				.get(configProperties.getHost().getTestDataHost() + "/cms/testdata/prerequisities").then().extract()
				.response();

		return response;
	}

	@Override
	public Response getLanguages() {
		
		Response response = given().header(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8")
				.get(configProperties.getHost().getRefDataHost() + "/api/referencedata/language").then().extract()
				.response();

		return response;
	}

	@Override
	public Response getLanguageByCode(String code) {
		
		Response response = given().header(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8").pathParam("code", code)
				.get(configProperties.getHost().getRefDataHost() + "/api/referencedata/language/{code}").then().extract()
				.response();

		return response;
	}

	@Override
	public Response getLanguageById(Long id) {
		
		Response response = given().header(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8").pathParam("id", id)
				.get(configProperties.getHost().getRefDataHost() + "/api/referencedata/languages/{id}").then().extract()
				.response();

		return response;
	}

	@Override
	public Response getCounries() {
		
		Response response = given().header(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8")
				.get(configProperties.getHost().getRefDataHost() + "/api/referencedata/country").then().extract()
				.response();

		return response;
	}

	@Override
	public Response getCountryById(Long id) {
		
		Response response = given().header(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8").pathParam("id", id)
				.get(configProperties.getHost().getRefDataHost() + "/api/referencedata/country/{id}").then().extract()
				.response();

		return response;
	}

	@Override
	public Response getRatings() {
		
		Response response = given().header(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8")
				.get(configProperties.getHost().getRefDataHost() + "/api/referencedata/ratings").then().extract()
				.response();

		return response;
	}

	@Override
	public Response getAgeRating() {
		
		Response response = given().header(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8")
				.get(configProperties.getHost().getRefDataHost() + "/api/referencedata/ratings/ageRating").then().extract()
				.response();

		return response;
	}

	@Override
	public Response getRegionalAgeRating() {
		Response response = given().header(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8")
				.get(configProperties.getHost().getRefDataHost() + "/api/referencedata/ratings/ageRating/regional").then().extract()
				.response();

		return response;
	}

	@Override
	public Response getRegion() {
		
		Response response = given().header(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8")
				.get(configProperties.getHost().getRefDataHost() + "/api/referencedata/region").then().extract()
				.response();

		return response;
	}

	@Override
	public Response getRegionsForAgerating() {
		
		Response response = given().header(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8")
				.get(configProperties.getHost().getRefDataHost() + "/api/referencedata/region/ageRating").then().extract()
				.response();

		return response;
	}

	@Override
	public Response getSubtitle(Long id) {

		Response response = given().header(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8").queryParams("cids",id)
				.get(configProperties.getHost().getClipHost() + "/api/clipService/subtitles").then().extract()
				.response();

		return response;
	}

	@Override
	public Response getSubtitleByType(Long id,String type) {
		
		Response response = given().header(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8").header("Authorization", "admin")
				.when().queryParam("type", type).pathParam("id", id)
				.get(configProperties.getHost().getClipHost() + "/api/clipService/cid/{id}/subtitle").then().extract()
				.response();

		return response;
	}

	@Override
	public Response pushClipToCaas(Long id) {
	
		Response response = given().header(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8").pathParam("id", id)
				.get(configProperties.getHost().getClipHost() + "/api/clipService/clip/push/{id}").then().extract()
				.response();

		return response;
	}

	@Override
	public Response pushClipToContext(Long id) {
		
		Response response = given().header(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8").queryParam("cid", id)
				.post(configProperties.getHost().getClipHost() + "/api/clipService/context").then().extract()
				.response();

		return response;
	}

	@Override
	public Response getRightsById(Long id) {
		
		Response response = given().header(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8").pathParam("id", id)
				.get(configProperties.getHost().getClipHost() + "/api/clipService/right/{id}").then().extract()
				.response();

		return response;
	}



}
