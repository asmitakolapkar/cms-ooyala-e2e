package com.vuclip.cms2.helper;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.JsonObject;
import com.vuclip.cms2.dao.CountryRepository;
import com.vuclip.cms2.entity.CountryEntity;
import com.vuclip.cms2.services.RestClientService;
import com.vuclip.cms2.util.ConfigProperties;

import io.restassured.response.Response;

@Component
public class CountryHelper extends BaseHelper{

	@Autowired
	RestClientService restClientService;

	@Autowired
	ConfigProperties configProperties;

	@Autowired
	CountryRepository countryRepo;
	
	@Transactional
	public void truncateCountry() {
		
		countryRepo.truncateCountry();
	}

	public CountryEntity getCountry(Long id) throws InterruptedException {

		Optional<CountryEntity> countryEntity = countryRepo.findById(id);

		for (int i = 0; i < 10; i++) {

			if (!countryEntity.isPresent() || 
					(System.currentTimeMillis() - countryEntity.get().getUpdatetime().getTime() < 5000L)) {
				Thread.sleep(500);

				countryEntity = countryRepo.findById(id);

			} else {
				break;
			}
		}

		return countryEntity.get();
	}
	
	public Response getCountries() throws Exception {

		Response response = restClientService.getCounries();

		return response;

	} 
	
	public Response getCountryById(Long id) throws Exception {

		Response response = restClientService.getCountryById(id);

		return response;

	} 
	
	@Override
	public void pushInputPubsubMessage(JsonObject message) {

		restClientService.pushDataToPubsubTopic(configProperties.getPubsub().getCountryTopic(), message);

	}
	
}
