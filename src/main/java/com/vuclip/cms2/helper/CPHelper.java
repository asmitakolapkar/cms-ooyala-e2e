package com.vuclip.cms2.helper;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.JsonObject;
import com.vuclip.cms2.dao.ContentPartnerRepository;
import com.vuclip.cms2.dao.RmmGenericRepository;
import com.vuclip.cms2.entity.ContentPartnerEntity;
import com.vuclip.cms2.entity.RmmGenericEntity;
import com.vuclip.cms2.services.RestClientService;
import com.vuclip.cms2.util.ConfigProperties;

/**
 * Asmita Kolapkar
 * 
 * 20-Jun-2019
 * 
 **/

@Component
public class CPHelper extends BaseHelper {

	@Autowired
	RestClientService restClientService;

	@Autowired
	ConfigProperties configProperties;

	@Autowired
	RmmGenericRepository rmmGenericRepo;

	@Autowired
	ContentPartnerRepository contentPartnerRepo;
	
	@Transactional
	public void truncateCpTables() {

		//contentPartnerRepo.truncateContentPartner();
		rmmGenericRepo.truncateRmmGeneric();
	}

	public RmmGenericEntity getRmmGenericData(Long id) throws InterruptedException {

		Optional<RmmGenericEntity> rmmGenericEntity = rmmGenericRepo.findByRmsid(id);

		for (int i = 0; i < 10; i++) {

			if (!rmmGenericEntity.isPresent() || 
					(System.currentTimeMillis() - rmmGenericEntity.get().getUpdatetime().getTime() < 5000L)) {
				Thread.sleep(500);

				rmmGenericEntity = rmmGenericRepo.findByRmsid(id);

			} else {
				break;
			}
		}

		return rmmGenericEntity.get();
	}
	
	public ContentPartnerEntity getCpData(Long id) throws InterruptedException {

		Optional<ContentPartnerEntity> cpEntity = contentPartnerRepo.findById(id);

		for (int i = 0; i < 10; i++) {

			if (!cpEntity.isPresent()) {
				Thread.sleep(500);

				cpEntity = contentPartnerRepo.findById(id);

			} else {
				break;
			}
		}

		return cpEntity.get();
	}
	
	@Override
	public void pushInputPubsubMessage(JsonObject message) {

		restClientService.pushDataToPubsubTopic(configProperties.getPubsub().getCpTopic(), message);
	}
}
