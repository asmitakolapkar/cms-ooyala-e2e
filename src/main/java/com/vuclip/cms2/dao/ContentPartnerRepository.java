package com.vuclip.cms2.dao;

import com.vuclip.cms2.entity.ContentPartnerEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface ContentPartnerRepository extends JpaRepository<ContentPartnerEntity, Long> {
    
	//ContentProviderEntity findByIdIn(Long contentId);
    
    @Modifying
	@Query(value = "truncate table content_partner", nativeQuery = true)
	public void truncateContentPartner();
}
