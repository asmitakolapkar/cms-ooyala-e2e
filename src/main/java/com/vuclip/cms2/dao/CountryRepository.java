package com.vuclip.cms2.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vuclip.cms2.entity.CountryEntity;

@Repository
public interface CountryRepository extends JpaRepository<CountryEntity, Long> {

	@Modifying
	@Query(value = "truncate table country", nativeQuery = true)
	public void truncateCountry();

	Optional<CountryEntity> findById(Long id);

}