package com.vuclip.cms2.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "ct_language")
@Getter
@Setter
public class LanguageEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	public Long Id;

	@Column(name = "code")
	public String code;

	@Column(name = "LANGUAGE")
	public String LANGUAGE;

	@Column(name = "language_id")
	public String language_id;

	@Column(name = "language_ar")
	public String language_ar;

	@Column(name = "language_ms")
	public String language_ms;

	@Column(name = "active")
	public String active;

	@Column(name = "updatetime")
	public Date updatetime;

}
