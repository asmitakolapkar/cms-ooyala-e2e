package com.vuclip.cms2.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@Entity(name = "rmm_generic")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RmmGenericEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long id;

	@Column(name = "cpid")
	public Long rmsid;

	@Column(name = "content_provider")
	public String name;

	@Column(name = "cp_manger")
	public String pri_manager_name;

	@Column(name = "sec_cp_manger")
	public String sec_manager_name;

	@Column(name = "system_name")
	public String systemName;

	@Column(name = "country")
	public String country;

	@Column(name = "email")
	public String opr_spoc_email;

	@Column(name = "contact")
	public String opr_spoc_contact;

	@Column(name = "spoc_name")
	public String opr_spocName;

	@Column(name = "status")
	public Integer status;

	@Column(name = "update_time")
	public Date updatetime;

	@Column(name = "create_time")
	public Date createtime;

	@Column(name = "business_spoc_name")
	public String bus_spocName;

	@Column(name = "business_email")
	public String bus_spoc_email;

	@Column(name = "business_contact")
	public String bus_spoc_contact;
}
