package com.vuclip.cms2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Cms2AutomationApplication {

	public static void main(String[] args) {
		SpringApplication.run(Cms2AutomationApplication.class, args);
	}

}
